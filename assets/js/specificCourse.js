const courseId = new URLSearchParams(document.location.search).get("courseId")

const cn = document.getElementById('courseName')
const desc = document.getElementById('desc')
const price = document.getElementById('price')
const enrollmentContainer = document.getElementById('enrollmentContainer')

let userToken = localStorage.getItem('token')

fetch(`http://localhost:3007/api/courses/${courseId}`, {
	method: "GET",
	headers: {
		"Authorization": `Bearer ${userToken}`
	}
})
.then(result => result.json())
.then(result => {
	console.log(result)

	cn.innerHTML = result.courseName
	desc.innerHTML = result.description
	price.innerHTML = result.price

	enrollmentContainer.innerHTML =
	`
		<button class="btn btn-outline-info btn-block" id="enrollButton">
			Enroll
		</button>
	`


	const enrollButton = document.getElementById('enrollButton')

	enrollButton.addEventListener("click", (e) => {
		e.preventDefault()

		fetch(`http://localhost:3007/api/users/enroll`,{
			method: "POST",
			headers: {
				"Content-Type": "application/json",
				"Authorization": `Bearer ${userToken}`
			},
			body: JSON.stringify({
				courseId: courseId
			})
		})
		.then(result => result.json())
		.then(result => {
			// console.log(result)

			if(result){
				alert('Course enrolled successfully.')

				window.location.replace('./courses.html')
			} else {
				alert('Something went wrong.')
			}
		})
	})
})
